![Logo](https://raw.github.com/MehdiK/Humanizer/master/logo.png)

Humanizer.jvm is an adaptation of the humanizer framework for .NET which is made for the JVM and is written in Kotlin.
Humanizer.jvm meets all your JVM needs for manipulating and displaying strings, enums, dates, times, timespans, numbers and quantities.

[Original Repo](https://github.com/MehdiK/Humanizer.jvm)

## What is this repo?

This repo is somewhat of a "fork" of the original GitHub one.

The original repo hasn't been updated for >4 years as of writing this (2019-01-24), and it contains numerous errors, both in gradle build file, and in the actual project. *(This is because it used a very early version of Kotlin, and features that it utilized have been renamed/removed since then)*

The version in this repo has been updated to work with **at least** `Kotlin v1.3.20`.

All of the tests have also been ported over to [KotlinTest](https://github.com/kotlintest/kotlintest/) rather than the *very* old version of Spek that it was using.

All credits go to [Mehedik](https://github.com/MehdiK) and any other contributors to the original repo.

## Why is it archived?

Because I'm not going to maintain **this** variation of it any further than this, if you want to add more features / fix bugs for this version, fork this and get tinkering.

## License

Due to the nature of this repo, it has the same license as the original one, which is that of Apache License 2.0.

## Downloads

Head over to the releases page.